﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Web;

namespace scginet
{
	public class ScgiChild
	{
		#region Fields
		private TcpClient c;
		private Stream s;
		private NameValueCollection headers = new NameValueCollection();
		private byte[] body;
		#endregion

		#region Properties
		public NameValueCollection Headers
		{
			get { return this.headers; }
		}

		public byte[] Body
		{
			get { return this.body; }
		}

		public Stream Response
		{
			get { return this.s; }
			set { this.s = value; }
		}
		#endregion

		#region Constructors
		public ScgiChild(TcpClient c)
		{
			this.c = c;
			this.s = c.GetStream();

			this.parseHeaders();
			this.getBody();
		}
		#endregion

		#region Methods
		public void Close()
		{
			this.s.Close();
			this.c.Close();
		}

		public bool UriIsWellFormatted()
		{
			return Uri.IsWellFormedUriString(this.Headers["REQUEST_URI"], UriKind.Relative);
		}

		public NameValueCollection ParseQueryString()
		{
			return HttpUtility.ParseQueryString(this.Headers["QUERY_STRING"]);
		}

		public NameValueCollection ParsePostBody()
		{
			return HttpUtility.ParseQueryString(Encoding.UTF8.GetString(this.Body));
		}

		private void parseHeaders()
		{
			if (this.headers.Count > 0)
				return;

			string[] data = this.s.ReadNetString().Split((char)0);
			for (int i = 0; i < (data.Length - 1); i+=2)
				this.headers.Add(data[i], data[i+1]);
		}

		private void getBody()
		{
			if (this.headers.Count <= 0)
				this.parseHeaders();

			int length = int.Parse(this.headers["CONTENT_LENGTH"], CultureInfo.InvariantCulture);
			byte[] buffer = new byte[length];
			int offset = 0;
			while (offset < length)
			{
				int nr = this.s.Read(buffer, offset, length - offset);
				offset += nr;
			}

			this.body = buffer;
		}
		#endregion
	}
}
