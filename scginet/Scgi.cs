﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace scginet
{
	public class Scgi
	{
		#region Fields
		private TcpListener listener;
		private Thread main;
		private bool active;
		#endregion

		#region Constructors
		public Scgi(int Port) : this(IPAddress.Loopback, Port)
		{ }

		public Scgi(IPAddress EndPoint, int Port)
		{
			this.listener = new TcpListener(EndPoint, Port);
		}
		#endregion

		#region Events
		public event EventHandler<EventArgs> Request;
		#endregion

		#region Methods
		public void Start()
		{
			this.active = true;

			this.listener.Start();

			this.listener.BeginAcceptTcpClient(client, this.listener);
		}

		public void Stop()
		{
			this.active = false;
			this.main.Abort();
			this.listener.Stop();
		}
		#endregion

		#region Threads
		private void client(IAsyncResult ar)
		{
			TcpClient c = this.listener.EndAcceptTcpClient(ar);

			this.listener.BeginAcceptTcpClient(client, this.listener);

			this.child(c);
		}

		private void child(object o)
		{
			TcpClient c = o as TcpClient;

			if (c == null)
				throw new InvalidDataException();

			ScgiChild sc = new ScgiChild(c);
			if (this.Request != null)
				this.Request(sc, new EventArgs());
		}
		#endregion
	}
}
