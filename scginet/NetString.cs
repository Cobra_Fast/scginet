﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace scginet
{
	internal static class NetString
	{
		#region Extension Methods
		internal static string ReadNetString(this Stream s)
		{
			int length = 0;

			while (true)
			{
				int c = s.ReadByte();

				if (c == ':')
					break;

				if (c < '0' && c > '9')
					throw new InvalidDataException();

				length = (length * 10) + (c - '0');
			}

			byte[] buffer = new byte[length];
			int offset = 0;
			while (offset < length)
			{
				int nr = s.Read(buffer, offset, length - offset);

				if (nr == 0)
					throw new EndOfStreamException();

				offset += nr;
			}

			if (s.ReadByte() != ',')
				throw new InvalidDataException();

			return Encoding.UTF8.GetString(buffer);
		}
		#endregion
	}
}
